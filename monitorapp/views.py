# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import re
import os

# from django.shortcuts import render

from utils.decorators import json_response


@json_response
def memory(request):

    meminfo = {}
    pattern = re.compile('([\w\(\)]+):\s*(\d+)(:?\s*(\w+))?')

    file_path = './test_memory.txt' if os.name != 'posix' else '/proc/meminfo'
    with open(file_path) as f:
        for line in f:
            match = pattern.match(line)
            if match:
                # For now we don't care about units (match.group(3))
                meminfo[match.group(1)] = float(match.group(2))
    return {
        'success': True,
        'meminfo': meminfo,
    }
